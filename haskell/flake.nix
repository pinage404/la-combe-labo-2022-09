{
  description = "Haskell with formatting, linting and test";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages."${system}";
        ghc = pkgs.haskellPackages.ghcWithPackages (haskellPackages: with haskellPackages; [
          fourmolu
          hlint
          hspec
        ]);
      in
      {
        devShell = pkgs.mkShell {
          packages = [
            ghc
            pkgs.haskellPackages.haskell-language-server # for vscode
          ];
        };
      });
}
