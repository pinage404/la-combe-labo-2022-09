{
  description = "Python with formatting, typing and test";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages."${system}";
        pythonEnv = pkgs.python310.withPackages (pythonPackages: [
          pythonPackages.black
          pythonPackages.mypy
          pythonPackages.pytest
        ]);
      in
      {
        devShell = pkgs.mkShell {
          packages = [
            pythonEnv
          ];
        };
      });
}
