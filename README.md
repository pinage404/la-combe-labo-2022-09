# Nix Sandboxes

Sandboxes / starters for differents languages

## Usage

* Install [Nix](https://nixos.org)
* Install [DirEnv](https://direnv.net)
* Install [Git](https://git-scm.com)
* (Optional) Install [VSCode](https://code.visualstudio.com)
* Clone this repository

  ```sh
  git clone https://gitlab.com/pinage404/nix-sandboxes.git
  cd nix-sandboxes
  ```

* Change to the directory to active the sandbox

  e.g. for Rust

  ```sh
  cd rust
  ```

* Enjoy

---

Fork of [`FaustXVI/sandboxes`](https://github.com/FaustXVI/sandboxes.git)
